// COMP5911M Coursework 1, Task 1

package ase.cwk1;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

import java.io.PrintWriter;
import java.util.ArrayList;


public class Report {
  private Writer output;


  public Report(Writer output) {
    this.output = output;
  }

  // 单一职责
  // why use StringBuilder in stead of output.write()
  // StringBuilder：No need to splice strings， Method area->Pool of string constants;
  // why use StringBuilder in stead of StringBuffer
  public void print(List<Machine> machines, Robot robot) throws IOException {
    //String a = "a" + "b" + "c";
    // a
    // b
    // ab
    // c
    // abc
    // StringBuffer synchronized lock;
    StringBuilder sb = new StringBuilder();
    sb.append("FACTORY REPORT\n\n");
    printMachines(machines, sb);
    sb.append(robot);
    sb.append("\n\nEND\n");
    output.write(sb.toString());
  }

  
  public void printMachines(List<Machine> machines, StringBuilder sb) {
    for (Machine machine: machines) {
      sb.append(machine);
    }
    sb.append("\n");
  }

}
