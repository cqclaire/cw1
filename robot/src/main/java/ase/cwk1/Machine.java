// COMP5911M Coursework 1, Task 1

package ase.cwk1;


public class Machine {
  private String name;
  private String location;
  private String item;

  public Machine(String name, String location) {
    this.name = name;
    this.location = location;
  }

  public String getName() {
    return name;
  }

  public String getLocation() {
    return location;
  }

  public String getItem() {
    return item;
  }

  public void addItem(String item) {
    this.item = item;
  }

  public String removeItem() {
    String removed = item;
    item = null;
    return removed;
  }

  /**
   * this class define the Format of the output
   * @return
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("Machine ").append(this.getName());
    if (this.getItem() != null) {
      sb.append(" item=").append(this.getItem());
    }
    sb.append("\n");
    return sb.toString();
  }

}
