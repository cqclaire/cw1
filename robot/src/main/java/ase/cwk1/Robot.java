// COMP5911M Coursework 1, Task 1

package ase.cwk1;


public class Robot {
  private Machine location;
  private String item;

  public Machine getLocation() {
    return location;
  }

  public void moveTo(Machine machine) {
    location = machine;
  }

  public String getItem() {
    return item;
  }

  public void pick() {
    item = location.removeItem();
  }

  public void release() {
    location.addItem(item);
    item = null;
  }

  /**
   * this class define the Format of the output
   * @return
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("Robot");
    if (this.getLocation() != null) {
      sb.append(" location=").append(this.getLocation().getName());
    }
    if (this.getItem() != null) {
      sb.append(" item=").append(this.getItem());
    }
    return sb.toString();
  }
}
