// COMP5911M Coursework 1, Task 2

package ase.cwk1;

import java.util.ArrayList;


public class Queue {
  private ArrayList<String> storage = new ArrayList<>();

  public int size() {
    return storage.size();
  }

  public void addToBack(String item) {
    storage.add(item);
  }

  public String removeFromFront() {
//    String item = storage.get(0);
    return storage.remove(0);  // Remove some unnecessary code
//    return item;
  }
}
