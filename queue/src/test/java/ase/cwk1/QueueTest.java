// COMP5911M Coursework 1, Task 2

package ase.cwk1;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class QueueTest {
  private Queue queue;

  @BeforeEach
  void setUp() {
    queue = new Queue();
  }

  @Test
  void queueInitiallyEmpty() {
    assertThat(queue.size(), is(0));
  }

  @Test
  void addingAnItem() {
    queue.addToBack("item1");
    assertThat(queue.size(), is(1));
  }

  @Test
  void removingItems() {
    queue.addToBack("item1");
    queue.addToBack("item2");
    assertAll(
      () -> assertThat(queue.removeFromFront(), is("item1")),
      () -> assertThat(queue.size(), is(1)),
      () -> assertThat(queue.removeFromFront(), is("item2")),
      () -> assertThat(queue.size(), is(0))
    );
  }
}
